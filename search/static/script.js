$(document).ready(() => {
    $('#buttonsearch').click(function(){
        var key = $('#searchbox').val();

        $.ajax({
            method: 'GET',
            url: 'getData?q=' + key,
            success: function(response){
                console.log(response)
                $("table").empty();
                $("table").append(
                    '<thead>' +
                    '<tr>' +
                    '<th scope="col">Cover</th>' +
                    '<th scope="col">Title</th>' +
                    '<th scope="col">Author</th>' +
                    '<th scope="col">Description</th>' +
                    '</tr>'+
                    '</thead>'
                );
                for(let i=0; i<response.items.length; i++){
                    $("table").append(
                        "<tbody>" +
                        "<tr>" +
                        "<th scope='row'>" + "<img src= '"+ response.items[i].volumeInfo.imageLinks.smallThumbnail  + "'>" + "</td>" +
                        "<td>" + response.items[i].volumeInfo.title + "</td>" +
                        "<td>" + response.items[i].volumeInfo.authors  + "</td>" +
                        "<td>" + response.items[i].volumeInfo.description  + "</td>" +
                        "</tr>" +
                        "</tbody>")

                }
            }
        })
    })
})