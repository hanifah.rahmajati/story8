from django.shortcuts import render
from django.http import JsonResponse
import requests
# Create your views here.
def searchhtml(request):
    return render(request, 'searchbook.html')

def searchajax(request):
    q = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + q

    response = requests.get(url)
    responseJson = response.json()
    return JsonResponse(responseJson)