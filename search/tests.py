from django.test import TestCase, Client
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class TestSearchPage(TestCase) :
    def test_url_search_ada(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_url_hasil_ada(self):
        response = Client().get('/getData?q=love')
        self.assertEqual(response.status_code, 200)

    def test_html_status(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'searchbook.html')

    def test_ada_kata(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("Go find your favorite book!", content)
    
    def test_ada_table(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("<table", content)
    
    
class FunctionalTestSearch(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(1)
        super(FunctionalTestSearch,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTestSearch, self).tearDown()
    
    def test_search_bekerja(self):
        self.browser.get('http://127.0.0.1:8000/')

        searchbox = self.browser.find_element_by_id("searchbox")
        searchbox.send_keys("Love")

        time.sleep(2)
        
        searchbutton = self.browser.find_element_by_id("buttonsearch")
        searchbutton.click()

        time.sleep(2)

        self.assertIn('Book Love', self.browser.page_source)
        self.assertIn('Penny Kittle', self.browser.page_source)
        self.assertIn('Presents strategies for getting students to read, and offers advice on building a school culture around a love of reading, helping students deepen their understanding of what they read, and balancing independent reading and text study.', self.browser.page_source)
        self.assertIn('<td', self.browser.page_source)
        self.assertIn('<img', self.browser.page_source)
        time.sleep(2)